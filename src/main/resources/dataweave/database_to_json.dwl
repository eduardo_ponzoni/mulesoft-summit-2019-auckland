%dw 2.0
output application/json
---
payload map (item, index) -> {
	registrationNumber: item.REG_NUM,
	name: item.SPEAKER,
	title: item.TITLE,
	company: item.COMPANY,
	details: item.DETAILS
}