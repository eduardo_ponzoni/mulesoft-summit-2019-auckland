##Automated MuleSoft Applications Testing

This is a very simple and basic application developed to be presented during the MuleSoft Summit 2019 in Auckland.

The solution suggests an approach to test of MuleSoft applications by implementing the concept of automated test pyramid (unit tests, component tests, end to end tests).
