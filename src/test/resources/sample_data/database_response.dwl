%dw 2.0
output application/java
---
[
  {
    REG_NUM: "P0NZ4BR451L",
    SPEAKER: "Eduardo Ponzoni",
    TITLE: "MuleSoft Practice Lead",
    COMPANY: "Datacom Systems",
    DETAILS: "Track 2: Take A Deep Dive"
  },{
    REG_NUM: "P3DR0BR451L",
    SPEAKER: "Pedro Tokarski",
    TITLE: "Lead Solutions Engineer",
    COMPANY: "MuleSoft",
    DETAILS: "Track 1: Getting Started"
  }
]