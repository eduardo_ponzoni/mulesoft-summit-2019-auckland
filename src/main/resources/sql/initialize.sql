CREATE TABLE SPEAKERS (
	REG_NUM	NVARCHAR(15)	NOT NULL PRIMARY KEY,
	SPEAKER	NVARCHAR(50)	NOT NULL,
	TITLE	NVARCHAR(50)	NOT NULL,
	COMPANY	NVARCHAR(50)	NOT NULL,
	DETAILS	NVARCHAR(300)	NOT NULL
);

INSERT INTO SPEAKERS (REG_NUM, SPEAKER, TITLE, COMPANY, DETAILS) VALUES ('P0NZ4BR451L', 'Eduardo Ponzoni', 'MuleSoft Practice Lead', 'Datacom Systems', 'Track 2: Take A Deep Dive');
INSERT INTO SPEAKERS (REG_NUM, SPEAKER, TITLE, COMPANY, DETAILS) VALUES ('P3DR0BR451L', 'Pedro Tokarski', 'Lead Solutions Engineer', 'MuleSoft', 'Track 1: Getting Started');
INSERT INTO SPEAKERS (REG_NUM, SPEAKER, TITLE, COMPANY, DETAILS) VALUES ('ADR14N5tJ0HN', 'Adrian Kryzewski', 'Principle Enterprise Architect', 'St John New Zealand', 'Customer Spotlight: St John New Zealand');
INSERT INTO SPEAKERS (REG_NUM, SPEAKER, TITLE, COMPANY, DETAILS) VALUES ('T0NY5tJ0HN', 'Tony Vodanovich', 'Head if Architecture & Security', 'St John New Zealand', 'Customer Spotlight: St John New Zealand');